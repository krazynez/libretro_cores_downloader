#!/usr/bin/env python3
'''
Author: Brennen Murphy
Date:   December 16, 2019
Version: 0.01
Contact: brennenmurph@pm.me / Discord: https://discord.gg/pAh6Cpv / GitLab: https://gitlab.com/krazynez
'''
from bs4 import BeautifulSoup as bs
import requests, re, os, wget, zipfile, sys
from urllib.parse import urlparse

os.chdir('cores')
page = requests.get('https://buildbot.libretro.com/nightly/linux/x86_64/latest/')
soup = bs(page.text, 'html.parser')

files = soup.find_all('li')
url_list = []
counter = 1

for link in soup.findAll('a', attrs={'href': re.compile("^/nightly/linux/x86_64/latest/")}):
    url = "https://buildbot.libretro.com"+link.get('href')
    f = urlparse(url)
    filename = os.path.basename(f.path)
    url_list.append(url)
    total = len(os.listdir()) - 1
    if total == len(url_list):
        print('\nFiles already downloaded. Please do not spam downloading.\n')
        sys.exit()
    else:
        pass
for _file in url_list:
    print(f'\n\n{counter}','/',len(url_list), 'files')
    counter = counter + 1
    wget.download(_file)
    with zipfile.ZipFile(os.path.basename(urlparse(_file).path), 'r') as zip_ref:
        current_file = os.path.basename(urlparse(_file).path)
        print(f'\n\n{current_file}','unzipping in cores folder')
        zip_ref.extractall('.')
        os.remove(os.path.basename(urlparse(_file).path))
