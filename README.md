# Libretro Core Downloader for Linux

_Please **DO NOT** abuse libretro servers! Only use this if necessary. We do not want to D/DOS the nice people that provide these servers._

## Setup

_Would suggest using a virtualenv for python for this._

    pip install -r requirements.txt


## Running

    python3 runmy.py
