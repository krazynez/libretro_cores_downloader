# Near Future Plans
 - List all cores (Prior to downloading)
 - Download select cores (lighter on servers)
 - Download groups/range of cores
 - Unix/Windows Support (Auto Detect, should be easy)


# Extended Future Plans
- GUI (Probably PyQt)
- Mirror(s) for servers to allow libretro to not take the brunt of downloads
